#!/usr/bin/env make

include .env
export $(shell sed 's/=.*//' .env)

.PHONY: default

default: prepareX11

prepareX11:
	rm -rf ${XAUTH}
	touch ${XAUTH}
	xauth nlist ${DISPLAY} | sed -e 's/^..../ffff/' | xauth -f ${XAUTH} nmerge -

