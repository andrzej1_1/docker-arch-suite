# Crossbar

If you want to install additional __pip__ or __pacman__ packages on startup, then set .env file accordingly, for example:

~~~
PACMAN_PACKAGES=telnet wget 
PIP_PACKAGES=django
~~~
