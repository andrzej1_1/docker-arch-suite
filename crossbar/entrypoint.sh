#!/usr/bin/env bash

# UID/GID map to unknown user/group, $HOME=/ (the default when no home directory is defined)
eval $( fixuid )
# UID/GID now match user/group, $HOME has been set to user's home directory

# install additional pacman packages
if [[ -n "${PACMAN_PACKAGES}" ]]; then

    need_installation=false
    for package in ${PACMAN_PACKAGES}
    do
        # check if package is not installed
        pacman -Qi "$package" &> /dev/null
        if [ $? -eq 1 ]; then
            need_installation=true
            break
        fi
    done

    if [ "$need_installation" = true ] ; then
        su-exec root pacman -Syy
        su-exec root pacman -S --needed --noconfirm ${PACMAN_PACKAGES}
    fi
fi

# install additional packages
if [[ -n "${PIP_PACKAGES}" ]]; then
    su-exec root pip install ${PIP_PACKAGES}
fi

if [ "$1" = 'crossbar' -a "$(id -u)" = '0' ]; then
    # initialize a Crossbar.io node
	chown -R crossbar:crossbar /node
	exec su-exec crossbar "$@"
fi

# Run CMD
exec "$@"
