#!/usr/bin/env bash

# UID/GID map to unknown user/group, $HOME=/ (the default when no home directory is defined)
eval $( fixuid )
# UID/GID now match user/group, $HOME has been set to user's home directory

# Add managment user
/opt/wildfly/bin/add-user.sh root root

# Execute CMD
exec "$@"
