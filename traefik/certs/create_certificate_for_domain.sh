#!/usr/bin/env bash

if [ -z "$1" ]
then
  echo "Please supply a subdomain to create a certificate for";
  echo "e.g. www.mysite.com"
  exit;
fi

if [ ! -f rootCA.pem ]; then
  echo 'Please run "create_root_cert_and_key.sh" first, and try again!'
  exit;
fi
if [ ! -f v3.ext ]; then
  echo 'Please download the "v3.ext" file and try again!'
  exit;
fi

DOMAIN=$1
COMMON_NAME=${2:-*.$1}
SUBJECT="/C=CA/ST=None/L=NB/O=None/CN=$COMMON_NAME"
NUM_OF_DAYS=999
openssl req -new -newkey rsa:2048 -sha256 -nodes -keyout "$DOMAIN.key" -subj "$SUBJECT" -out "$DOMAIN.csr"
cat v3.ext | sed s/%%DOMAIN%%/"$DOMAIN"/g > /tmp/__v3.ext
openssl x509 -req -in "$DOMAIN.csr" -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out "$DOMAIN.crt" -days $NUM_OF_DAYS -sha256 -extfile /tmp/__v3.ext

echo 
echo "###########################################################################"
echo Done! 
echo "###########################################################################"
echo "Certificate for $DOMAIN generated. Now you can use these files:"
echo "    - private key:    $DOMAIN.key"
echo "    - public key:     $DOMAIN.crt"
