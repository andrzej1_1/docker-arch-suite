#!/usr/bin/env bash
SUBJECT="/C=CA/ST=None/L=NB/O=None/CN=DAS"
openssl genpkey -out rootCA.key -algorithm RSA -pkeyopt rsa_keygen_bits:4096
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -subj "$SUBJECT" -out rootCA.pem
