# Apache python3

If you want to install additional __pip__ or __pacman__ packages on startup, then set .env file accordingly, for example:

~~~
PACMAN_PACKAGES=telnet wget 
PIP_PACKAGES=django
~~~

## Logs location

- /var/log/httpd/access_log
- /var/log/httpd/error_log

## Samples

- https://site1.apache-python3.test/
- https://site2.apache-python3.test/
