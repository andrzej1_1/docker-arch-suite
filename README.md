# docker-arch-suite

Service-oriented suite with all containers based on one Arch Linux.

--

Firstly add following lines to /etc/hosts for friendly urls

    127.0.1.1   site1.test
    127.0.1.1   site2.test
    127.0.1.1   site3.test

Then create network and start proxy service:

    $ docker network create nginxproxy_default
    $ docker-compose -f docker-proxy.yml up

Now you have following options:

- Start whole bundle

      $ docker-compose -f docker-common.yml up

- Start site 1

      $ docker-compose -f docker-common.yml -f php5/src/site1/docker-compose.yml up site1

- Start site 2

      $ docker-compose -f docker-common.yml -f php7/src/site2/docker-compose.yml up site2

- Start site 3

      $ docker-compose -f docker-common.yml -f php7/src/site3/docker-compose.yml up site3

---

Sites are accessible at following urls:

 - site 1 -> http://site1.test
 - site 2 -> http://site2.test
 - site 3 -> http://site3.test

## Services

### Reverse proxy

You can access traefik dashboard at __https://monitor.test__.

### Redis

Host: redis
Port: 6379

## Permissions

### Fixing

Set .env variables according to result of `id -u` and `id -g`.

### Story

Handling permissions with docker shared volumes is quite complicated (see https://github.com/moby/moby/issues/7198). I was trying different approaches: namespaces, mounting /etc/password and few others ways. Unfortunately all of them was primitive workarounds which did not meet base requirements. But finally I found a good one! [fixuid](https://github.com/boxboat/fixuid/tree/master/docker) comes to rescue and solves all problems without complicated configuration.

## Upgrading base image

If you want to (re)build containers some time after base image is pulled, then you might end up with pacman's 404 error. Solution is just to update base image with following command:

    $ docker pull archimg/base-devel

## Sharing display (X11)

If you want to run some GUI programs in containers then you have to share display with host. Of course you can do it easily :) So firstly run:

    $ make prepareX11

Then append to service config following volumes and environments:

~~~
volumes:
  ...
  - ${XSOCK}:${XSOCK}:rw                           
  - ${XAUTH}:${XAUTH}:rw               
environment:                       
  ...
  - DISPLAY=${DISPLAY}
  - XAUTHORITY=${XAUTH}
~~~

## Local HTTPS

1. Change directory to __traefik/certs__:
~~~
$ cd traefik/certs
~~~
2. Generate root certificates:
~~~
$ create_root_cert_and_key.sh
~~~
3. Import rootCA.pem in your key store (eg. Google Chrome)
4. Generate cert for local domain:
~~~
create_certificate_for_domain.sh yourdomain.test
~~~
5. Add following configuration to traefik/traefik.toml:
~~~
[[entryPoints.https.tls.certificates]]
certFile = "/certs/yourdomain.test.crt"
keyFile = "/certs/yourdomain.test.key"
~~~
